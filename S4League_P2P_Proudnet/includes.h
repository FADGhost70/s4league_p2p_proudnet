#include <WinSock2.h>

#include <windows.h>

#include <stdio.h>
#include <fcntl.h>
#include <io.h>

#include <time.h>

#include <vector>

#include "console.h"
#include "tools.h"
#include "vtableDetour.h"
#include "packet.h"
#include "sockMgr.h"
#include "p2p.h"
#include "proudpacket_P2P.h"
#include "p2pHandler.h"