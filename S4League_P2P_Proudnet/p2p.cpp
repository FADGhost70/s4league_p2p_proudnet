#include "stdafx.h"
CP2P*	g_P2P;

int WINAPI myWSASendTo(SOCKET s, LPWSABUF lpBuffers, DWORD dwBufferCount,  LPDWORD lpNumberOfBytesSent, DWORD dwFlags, sockaddr* lpTo, int iToLen, LPWSAOVERLAPPED lpOverlapped, LPWSAOVERLAPPED_COMPLETION_ROUTINE lpCompletionRoutine);
int WINAPI myWSARecvFrom(SOCKET s, LPWSABUF lpBuffers, DWORD dwBufferCount, LPDWORD lpNumberOfBytesRecvd, LPDWORD lpFlags, sockaddr* lpFrom, LPINT lpFromlen, LPWSAOVERLAPPED lpOverlapped, LPWSAOVERLAPPED_COMPLETION_ROUTINE lpCompletionRoutine);

CP2P::CP2P(void)
{
	__LOG(C_BLACK,     "",     "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	__LOG(C_BLACK,     "",     "                                              |");
	__LOG(C_LIGHTBLUE, "INFO", "            S4league P2P Logger               |");
	__LOG(C_LIGHTBLUE, "INFO", "           ~Console Class by xsh~             |");
	__LOG(C_LIGHTBLUE, "INFO", "           ~Project by FreehuntX~             |");
	__LOG(C_LIGHTBLUE,     "", "                                              |");
	__LOG(C_LIGHTBLUE, "INFO", "         ~Special thanks to Wtfblub~          |");
	__LOG(C_BLACK,     "",     "                                              |");
	__LOG(C_BLACK,     "",     "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	__LOG(C_BLACK,     "",     "");

	WSAAttach();
}


CP2P::~CP2P(void)
{
}

void CP2P::WSAAttach( )
{
	int aWSASendTo = (int)GetProcAddress(GetModuleHandle("WS2_32.dll"), "WSASendTo");
	DWORD vWSASendTo = g_Tools.MemoryscanInt(0x00400000, 0x1662000, aWSASendTo);
	if(vWSASendTo)
	{
		h_WSASendTo = new CVtableDetour( vWSASendTo );
		h_WSASendTo->WriteAddress( (DWORD)&myWSASendTo );

		__LOG(C_GREEN, "SUCCESS", "Detoured 'WSASendTo'.");
	}
	else
	{
		__LOG(C_RED, "FAIL", "Couldn't find 'WSASendTo'.");
	}
	
	int aWSARecvFrom = (int)GetProcAddress(GetModuleHandle("WS2_32.dll"), "WSARecvFrom");
	DWORD vWSARecvFrom = g_Tools.MemoryscanInt(0x00400000, 0x1662000, aWSARecvFrom);
	if(vWSARecvFrom)
	{
		h_WSARecvFrom = new CVtableDetour( vWSARecvFrom );
		h_WSARecvFrom->WriteAddress( (DWORD)&myWSARecvFrom );

		__LOG(C_GREEN, "SUCCESS", "Detoured 'WSARecvFrom'.");
	}
	else
	{
		__LOG(C_RED, "FAIL", "Couldn't find 'WSARecvFrom'.");
	}
}

void CP2P::WSADetach( )
{
	delete h_WSASendTo;
	delete h_WSARecvFrom;
}


int WINAPI myWSASendTo(SOCKET s,
                          LPWSABUF lpBuffers,
                          DWORD dwBufferCount,
                          LPDWORD lpNumberOfBytesSent,
                          DWORD dwFlags,
                          sockaddr* lpTo,
                          int iToLen,
                          LPWSAOVERLAPPED lpOverlapped,
                          LPWSAOVERLAPPED_COMPLETION_ROUTINE lpCompletionRoutine)
{
	g_SockMgr.SendHook( s, lpBuffers, dwBufferCount, lpOverlapped );

	return WSASendTo(s, lpBuffers, dwBufferCount, lpNumberOfBytesSent, dwFlags, lpTo, iToLen, lpOverlapped, lpCompletionRoutine);
}

int WINAPI myWSARecvFrom( SOCKET s,
                          LPWSABUF lpBuffers,
                          DWORD dwBufferCount,
                          LPDWORD lpNumberOfBytesRecvd,
                          LPDWORD lpFlags,
                          sockaddr* lpFrom,
                          LPINT lpFromlen,
                          LPWSAOVERLAPPED lpOverlapped,
                          LPWSAOVERLAPPED_COMPLETION_ROUTINE lpCompletionRoutine )
{
	int retn;

	retn = WSARecvFrom(s, lpBuffers, dwBufferCount, lpNumberOfBytesRecvd, lpFlags, lpFrom, lpFromlen, lpOverlapped, lpCompletionRoutine);

	g_SockMgr.RecvHook( s, lpBuffers, dwBufferCount, lpNumberOfBytesRecvd, lpOverlapped );

	return retn;
}