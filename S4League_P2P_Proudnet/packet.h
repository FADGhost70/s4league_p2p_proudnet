#pragma once
class CPacket
{
private:
	DWORD	m_buffer;
	DWORD	m_index;
	DWORD	m_headerSize;
	DWORD	m_size;
public:
	CPacket( void* buffer );
	~CPacket(void);

	void setIndex(int index);
	void skip( int nNum );
	DWORD getAddress() { return m_buffer + m_index; }
	DWORD get_index() { return m_index; }
	DWORD get_headerSize() { return m_headerSize; }
	DWORD get_size() { return m_size; }

	std::string readString( int size, bool movePosition = true )
	{
		std::string s;
		for( int i = 0; i < size; i++ )
		{
			s += *(char*)(m_buffer + m_index + i);
		}
		
		if( movePosition )
			m_index += size;

		return s;
	}
	
	BYTE readByte( bool movePosition = true )
	{
		BYTE retn = 0;

		retn = *(BYTE*)(m_buffer + m_index);

		if( movePosition )
			m_index += 1;

		return retn;
	}
	
	WORD readWord( bool movePosition = true )
	{
		WORD retn = 0;

		retn = *(WORD*)(m_buffer + m_index);

		if( movePosition )
			m_index += 2;

		return retn;
	}
	
	DWORD readDword( bool movePosition = true )
	{
		DWORD retn = 0;

		retn = *(DWORD*)(m_buffer + m_index);

		if( movePosition )
			m_index += 4;

		return retn;
	}

	template <class type>
	void read(type& buffer, bool movePosition = true)
	{
		size_t totalSize = sizeof(type);
 
		memcpy(&buffer, m_buffer + m_index, totalSize);
 
		if(movePosition)
			m_index += totalSize;
	}

	template <class type>
	void readArray(type* buffer, int size, bool movePosition = true)
	{
		size_t totalSize = sizeof(type) * size;

		memcpy(buffer, m_buffer + m_index, totalSize);

		if(movePosition)
			m_index += totalSize;
	}
};

