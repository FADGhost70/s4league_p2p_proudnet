#include "stdafx.h"

//This one is used, if you self want parse the packet outside the function. (Used for Send)
CProudpacket_P2P::CProudpacket_P2P( )
{
	this->header = reinterpret_cast<SProudnetheader_P2P*>(header);
}

CProudpacket_P2P::CProudpacket_P2P( void* buffer )
{
	int currentSize = 0;

	this->header = reinterpret_cast<SProudnetheader_P2P*>(buffer);

	//Here we fill the bodys Vector. Since there can be multiple Bodys in 1 Packet
	//currentSize is the actual readed bodysize. Payloadlen is the complete packet length
	while( currentSize < this->header->payloadLen ) {
		this->bodys.push_back( new CPacket( (void*)((DWORD)buffer + __PROUDHEADERSIZE + currentSize)) );
		currentSize += this->bodys.back()->get_size() + this->bodys.back()->get_headerSize();
	}
}


CProudpacket_P2P::~CProudpacket_P2P(void)
{
}
