#include "stdafx.h"
CSockMgr g_SockMgr;


CSockMgr::CSockMgr(void)
{
}


CSockMgr::~CSockMgr(void)
{
}

void CSockMgr::SendHook(SOCKET s, LPWSABUF lpBuffers, DWORD dwBufferCount, LPWSAOVERLAPPED lpOverlapped)
{	
	//This is not a valid Proudnet packet, because it dont have multiple buffer
	if( dwBufferCount <= 1 ) return;
	
	if( *(WORD*)(lpBuffers[0].buf) != __P2PMAGIC1 && *(WORD*)(lpBuffers[0].buf) != __P2PMAGIC2 )
		return;
	
	CProudpacket_P2P* p2pPacket = new CProudpacket_P2P( );
	p2pPacket->header = reinterpret_cast<SProudnetheader_P2P*>(lpBuffers[0].buf);

	for(int i = 1; i < (int)dwBufferCount; i++)
	{
		p2pPacket->bodys.push_back( new CPacket( lpBuffers[i].buf) );
	}
	
	g_P2PHandler.handlePacket( p2pPacket, DIR_OUT );
}

void CSockMgr::RecvHook(SOCKET s, LPWSABUF lpBuffers, DWORD dwBufferCount, LPDWORD numBytes, LPWSAOVERLAPPED lpOverlapped)
{
	for(int i = 0; i < (int)dwBufferCount; i++)
	{
		if( *(WORD*)(lpBuffers[i].buf) != __P2PMAGIC1 && *(WORD*)(lpBuffers[i].buf) != __P2PMAGIC2 )
			continue;

		CProudpacket_P2P* p2pPacket = new CProudpacket_P2P(lpBuffers[i].buf);
		g_P2PHandler.handlePacket( p2pPacket, DIR_IN );
	}
}