#pragma once
class CSockMgr
{
public:
	CSockMgr(void);
	~CSockMgr(void);

	void	SendHook(SOCKET s, LPWSABUF lpBuffers, DWORD dwBufferCount, LPWSAOVERLAPPED lpOverlapped);
	void	RecvHook(SOCKET s, LPWSABUF lpBuffers, DWORD dwBufferCount, LPDWORD numBytes, LPWSAOVERLAPPED lpOverlapped);
};

