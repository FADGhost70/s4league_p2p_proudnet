#include "stdafx.h"


CVtableDetour::CVtableDetour( DWORD address )
{
	m_address = address;
	m_realfunction = *(DWORD*)(address);
}


CVtableDetour::~CVtableDetour(void)
{
	*(DWORD*)(m_address) = m_realfunction; //VTable wieder zurücksetzen
}

void CVtableDetour::WriteAddress( DWORD address )
{
	*(DWORD*)(m_address) = address;
}