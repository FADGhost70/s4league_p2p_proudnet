#pragma once
class CVtableDetour
{
public:
	CVtableDetour( DWORD address );
	~CVtableDetour(void);
	void WriteAddress( DWORD address );
private:
	DWORD m_address;
	DWORD m_realfunction;
};

